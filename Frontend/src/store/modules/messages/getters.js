export default {
  messages(state, _, _2, rootGetters) {
    const userId = rootGetters.ownerId;
    console.log(userId);
    return state.messages.filter((mes) => mes.user == userId);
  },
  hasMessages(_, getters) {
    return getters.messages && getters.messages.length > 0;
  },
};
