const express = require("express");
const {
  getMessages,
  addMessage,
  getMessage,
  updateMessage,
  deleteMessage,
} = require("../controller/message");

const Message = require("../models/Message");
const advancedResults = require("../middleware/advancedResults");

const { protect, authorize } = require("../middleware/auth");

const router = express.Router({ mergeParams: true });

router
  .route("/")
  .get(advancedResults(Message), getMessages)
  .post(protect, authorize("admin", "homeOwner"), addMessage);
router
  .route("/:id")
  .get(getMessage)
  .put(protect, authorize("admin", "homeOwner"), updateMessage)
  .delete(protect, authorize("admin", "homeOwner"), deleteMessage);
module.exports = router;
