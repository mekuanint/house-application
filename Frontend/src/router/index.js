import { createRouter, createWebHistory } from "vue-router";
import store from "../store"
import HouseList from "../pages/houses/HouseList.vue";
import HouseDetail from "../pages/houses/HouseDetail.vue";
import NotFound from "../pages/NotFound.vue";
import Home from "../pages/home/Home.vue";
import Registration from "../pages/register/Registration.vue";
import Login from "../pages/register/Login.vue";
import NewHome from "../pages/houses/NewHome.vue";
import ReceivedMessages from "../pages/messages/ReceivedMessages.vue";
import HomeDistribution from "../pages/distribution/HomeDistribution.vue";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    { path: "/", redirect: "/home" },
    { path: "/home", component: Home },
    { path: "/detail", component: HouseDetail },
    { path: "/houses", component: HouseList },
    { path: "/distribution", component: HomeDistribution},
    { path: "/register", component: Registration },
    { path: "/addHouse", component: NewHome, meta: { requiresAuth: true } },
    {
      path: "/messages",
      component: ReceivedMessages,
      meta: { requiresAuth: true },
    },
    { path: "/login", component: Login },
    {
      path: "/houses/:id",
      props: true,
      component: HouseDetail,
      children: [
        {
          path: "contact",
          component: HouseDetail,
        },
      ],
    },
    { path: "/:notFound(.*)", component: NotFound },
  ],
});

router.beforeEach(function (to, _, next) {
  if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
    next("/login");
  } else {
    next();
  }
});

export default router;
