### Home Renting and Selling online application which can also shows the distribution of houses for living and apartments in the country

## it was made with frontend with vue3 and tailwind css
## and backend Mongo, Nodejs, and Express
## to run the application start two terminals and on the first terminal go to Frontend folder and 
1) npm install
2) npm run dev
## second on the Backend folder
1) to start the mongodb -> sudo systemctl start mongod

2) then npm start ---- KUDOS 🤺🤺🤺🤺🤺

## The home part of the application 
![Screenshot_from_2022-03-11_00-13-36](/uploads/e7aee48ffe4a9819c00d5084331a5f19/Screenshot_from_2022-03-11_00-13-36.png)

![Screenshot_from_2022-03-11_00-13-50](/uploads/68e56b13ef29607b71d91ce5935e3ea8/Screenshot_from_2022-03-11_00-13-50.png)

![Screenshot_from_2022-03-11_00-14-07](/uploads/5400033bf3bd9639630f1429ee78619a/Screenshot_from_2022-03-11_00-14-07.png)

![Screenshot_from_2022-03-11_00-14-14](/uploads/73af4028876dc2a193fa4b318db9653f/Screenshot_from_2022-03-11_00-14-14.png)

![Screenshot_from_2022-03-11_00-14-18](/uploads/65535995971644316e7f926edf6472d9/Screenshot_from_2022-03-11_00-14-18.png)

##The list of houses part of the application

![Screenshot_from_2022-03-11_00-14-26](/uploads/f3324fa06e52f3ee44953e89c14598a8/Screenshot_from_2022-03-11_00-14-26.png)

## Home destiribution in the cities part of the application 

![Screenshot_from_2022-03-11_00-14-45](/uploads/35a688e3581d66b7d4dc51e2f01e2d59/Screenshot_from_2022-03-11_00-14-45.png)

## Login and Register parts of the application

![Screenshot_from_2022-03-11_00-15-07](/uploads/f5ef61350bc8f3aaa3eaaeec24673bdb/Screenshot_from_2022-03-11_00-15-07.png)

![Screenshot_from_2022-03-11_00-15-15](/uploads/42711794f16a91383c5f4528c85a8c7b/Screenshot_from_2022-03-11_00-15-15.png)
