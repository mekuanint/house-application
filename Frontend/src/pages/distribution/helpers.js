export const mapOptionsBase = {
    fullscreenControl: false,
    mapTypeControl: false,
    rotateControl: false,
    scaleControl: false,
    streetViewControl: false,
    zoomControl: false,
    zoom: 2
  };
  
  export const mapOptions = {
    ...mapOptionsBase,
    center: { lat: -27, lng: 133 },
    zoom: 4
  };
  
  export const ausBounds = {
    north: -12,
    east: 155,
    south: -40,
    west: 113
  };
  
  export const icon = {
    path: "M -2,0 0,-2 2,0 0,2 z",
    strokeColor: "#F00",
    fillColor: "#F00",
    fillOpacity: 1
  };
  export const icons = [
    { icon, offset: "0%" },
    { icon, offset: "100%" }
  ];
  