import { createApp } from "vue";
import v3gmaps from "v3-gmaps";
import App from "./App.vue";
import "./style.css";
import router from "./router/index.js";
import store from "./store/index.js";
import BaseButton from "./components/UI/BaseButton.vue";
const app = createApp(App);
import "v3-gmaps/dist/style.css";

app.component("base-button", BaseButton);
app.use(router);
app.use(v3gmaps, { key: "AIzaSyC9sOnj0Up1QW-2Kb32GOCdhAUPcAV5pJI" });
app.use(store);
app.mount("#app");
