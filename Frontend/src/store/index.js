import { createStore } from "vuex";
import appartmentModule from "./modules/appartment/index.js";
import messagesModule from "./modules/messages/index.js";
const register_url = "http://localhost:5000/api/auth/register";
const login_url = "http://localhost:5000/api/auth/login";
const me_url = "http://localhost:5000/api/auth/me";
import axios from "axios";
const store = createStore({
  modules: {
    appartment: appartmentModule,
    messages: messagesModule,
  },
  state() {
    return {
      userId: null,
      token: null,
      user: null,
    };
  },
  getters: {
    ownerId(state) {
      return state.userId
    },
    token(state) {
      return state.token;
    },
    user(state) {
      return state.user;
    },
    isAuthenticated(state) {
      return !!state.token;
    },
  },
  mutations: {
    setUser(state, payload) {
      state.userId = payload;
    },
    registerOwner(state, payload) {
      state.token = payload;
    },
    login(state, payload) {
      state.token = payload;
    },
    setOwner(state, payload) {
      state.token = payload.token;
      state.userId = payload.userId;
    },
    setAll(state, payload) {
      state.user = payload;
    },
  },
  actions: {
    tryLogin(context) {
      const token = localStorage.getItem("token");
      const userId = localStorage.getItem("userId");
      if (token && userId) {
        context.commit("setOwner", {
          token: token,
          userId: userId,
        });
      }
    },
    logout(context) {
      localStorage.removeItem("token");
      localStorage.removeItem("userId");
      context.commit("login", null);
    },
    async login({ dispatch }, data) {
      const userData = {
        email: data.email,
        password: data.password,
      };
      const response = await axios.post(login_url, userData);

      dispatch("attemptLogin", response.data.token);
    },

    async attemptLogin({ commit }, token) {
      commit("login", token);
      try {
        let response = await axios.get(me_url, {
          headers: {
            authorization: "Bearer " + token,
          },
        });

        localStorage.setItem("token", token);
        localStorage.setItem("userId", response.data.data._id);
        commit("setUser", response.data.data._id);
        commit("setAll", response.data.data);
        console.log(response.data.data);
      } catch (e) {
        console.log("faild");
      }
    },

    async registerOwner({ dispatch }, data) {
      const ownerData = {
        firstName: data.f_name,
        lastName: data.l_name,
        role: "homeOwner",
        email: data.email,
        password: data.password,
      };

      const response = await axios.post(register_url, ownerData);
      dispatch("attempt", response.data.token);

      //context.commit("registerOwner", response.data);
    },

    async attempt({ commit }, token) {
      commit("registerOwner", token);

      try {
        let respo = await axios.get(me_url, {
          headers: {
            authorization: "Bearer " + token,
          },
        });
        commit("setUser", respo.data.data._id);
        commit("setAll", respo.data.data);
        console.log(respo.data);
      } catch (e) {
        console.log("faild");
      }
    },
  },
});

export default store;
