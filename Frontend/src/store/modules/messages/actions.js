import axios from "axios";
const post_url = "http://localhost:5000/api/messages";
const fetch_url = "http://localhost:5000/api/messages";
export default {
  async sendMessages(context, payload) {
    const newMessage = {
      user: payload.ownerId,
      fname: payload.fname,
      lname: payload.lname,
      email: payload.email,
      message: payload.message,
    };
    const token = context.rootGetters.token;
    const response = await axios.post(post_url, newMessage, {
      headers: {
        authorization: "Bearer " + token,
      },
    });
    context.commit("addMessage", response.data.data);
  },

  async loadMessages(context) {
    const response = await axios.get(fetch_url);
    context.commit("setMessages", response.data.data);
    console.log(response.data);
  },
};
