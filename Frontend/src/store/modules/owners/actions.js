import axios from "axios";
const register_url = "http://localhost:5000/api/auth/register";
const login_url = "http://localhost:5000/api/auth/login";
const me_url = "http://localhost:5000/api/auth/me";
export default {
  async login({dispatch}, data) {
      const userData = {
          email: data.email,
          password: data.password
      }
      const response = await axios.post(login_url, userData)

      dispatch('attemptLogin', response.data.token)
  },
  
  async attemptLogin({commit}, token){

    commit("login", token)
    try {
        let respo = await axios.get(me_url, {
          headers: {
            authorization: "Bearer " + token,
          },
        });
        commit("setUser", respo.data);
        console.log(respo.data);
      } catch (e) {
        console.log("faild");
      }


  },


  async registerOwner({ dispatch }, data) {
    const ownerData = {
      firstName: data.f_name,
      lastName: data.l_name,
      role: "homeOwner",
      email: data.email,
      password: data.password,
    };

    const response = await axios.post(register_url, ownerData);
    dispatch("attempt", response.data.token);

    //context.commit("registerOwner", response.data);
  },

  async attempt({ commit }, token) {
    commit("registerOwner", token);

    try {
      let respo = await axios.get(me_url, {
        headers: {
          authorization: "Bearer " + token,
        },
      });
      commit("setUser", respo.data);
      console.log(respo.data);
    } catch (e) {
      console.log("faild");
    }
  },
};
