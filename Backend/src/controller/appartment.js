const path = require("path");
const multer = require("multer");
const { nanoid } = require("nanoid");
const ErrorResponse = require("../utils/ErrorResponse");
const asyncHandler = require("../middleware/async");
const Appartment = require("../models/Appartment");
const advancedResults = require("../middleware/advancedResults");

// @desc      Get Appartments
// @route     GET /api/appartments
// @access    Public
exports.getAppartments = asyncHandler(async (req, res, next) => {
  if (req.params.shopId) {
    console.log(req.params.shopId);
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc      Get single Appartment
// @route     GET /api/Appartments/:id
// @access    Public
exports.getAppartment = asyncHandler(async (req, res, next) => {
  const appartment = await Appartment.findById(req.params.id);

  if (!appartment) {
    return next(
      new ErrorResponse(`No Appartment with the id of ${req.params.id}`),
      404
    );
  }
  res.status(200).json({
    success: true,
    data: appartment,
  });
});


// @desc      Create new Appartment
// @route     POST /api/appartments
// @access    Private
exports.addAppartment = asyncHandler(async (req, res, next) => {
  req.body.user = req.user.id;
  const appartment = await Appartment.create(req.body);
  res.status(201).json({
    success: true,
    data: appartment,
  });
});

// @desc      Update Appartment
// @route     PUT /api/Appartments/:id
// @access    Private
exports.updateAppartment = asyncHandler(async (req, res, next) => {
  let appartment = await Appartment.findById(req.params.id);

  if (!appartment) {
    return next(
      new ErrorResponse(`No Appartment with the id of ${req.params.id}`),
      404
    );
  }

  // Make sure user is appartment owner
  if (appartment.user.toString() !== req.user.id && req.shop.role !== "admin") {
    return next(
      new ErrorResponse(
        `User ${req.shop.id} is not authorized to update a Appartment with id ${req.params.id}`,
        401
      )
    );
  }

  appartment = await Appartment.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: appartment,
  });
});

// @desc      Delete Appartment
// @route     DELETE /api/Appartments/:id
// @access    Private
exports.deleteAppartment = asyncHandler(async (req, res, next) => {
  const appartment = await Appartment.findById(req.params.id);

  if (!appartment) {
    return next(
      new ErrorResponse(`No Appartment with the id of ${req.params.id}`),
      404
    );
  }

  // Make sure user is s appartment owner
  if (appartment.user.toString() !== req.user.id && req.user.role !== "admin") {
    return next(
      new ErrorResponse(
        `User ${req.shop.id} is not authorized to update a delete with id ${req.params.id}`,
        401
      )
    );
  }
  await appartment.remove();
  res.status(200).json({
    success: true,
    data: {},
  });
});

// @desc      Upload photo for home
// @route     PUT /api/apprtments/:id/photo
// @access    Private
exports.appartmentPhotoUpload = asyncHandler(async (req, res, next) => {
  const appartment = await Appartment.findById(req.params.id);

  if (!appartment) {
    return next(
      new ErrorResponse(`Appartment not found with id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is appartment owner
  if (appartment.user.toString() !== req.user.id && req.user.role !== "admin") {
    return next(
      new ErrorResponse(
        `User ${req.params.id} is not authorized to update this appartment`,
        401
      )
    );
  }

  if (!req.files) {
    return next(new ErrorResponse(`Please upload a file`, 400));
  }

  const file = req.files.file;

  // Make sure the image is a photo
  if (!file.mimetype.startsWith("image")) {
    return next(new ErrorResponse(`Please upload an image file`, 400));
  }

  // Check filesize
  if (file.size > process.env.MAX_FILE_UPLOAD) {
    return next(
      new ErrorResponse(
        `Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`,
        400
      )
    );
  }

  // Create custom filename
  file.name = `photo_${appartment._id}${path.parse(file.name).ext}`;

  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async (err) => {
    if (err) {
      console.error(err);
      return next(new ErrorResponse(`Problem with file upload`, 500));
    }

    await Appartment.findByIdAndUpdate(req.params.id, { image: file.name });

    res.status(200).json({
      success: true,
      data: file.name,
    });
  });
});
