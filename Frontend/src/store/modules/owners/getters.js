export default {
  ownerId(state) {
    return state.user.id;
  },
  token(state) {
    return state.token;
  },
};
