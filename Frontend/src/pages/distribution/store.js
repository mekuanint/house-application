import { ref } from "vue";

export const logs = ref([]);
export const clearLogs = () => (logs.value = []);
export const log = (s) => {
  // console.log(s)
  logs.value.unshift(s);
};
