export default {
  appartments(state) {
    return state.appartments;
  },
  users(state){
    return state.users
  },
  hasAppartments(state) {
    return state.appartments && state.appartments.length > 0;
  },
  isAppartment(_, getters, _2, rootGetters) {
    const appartments = getters.appartments;
    const userId = rootGetters.userId;
    return appartments.some((appartment) => appartment.id === userId);
  },
};
