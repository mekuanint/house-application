const mongoose = require("mongoose");
const messageFields = {
  f_name: {
    type: String,
  },
  l_name: {
    type: String,
  },
  email: {
    type: String,
  },
  message: {
    type: String,
  },
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true,
  },
};

const MessageSchema = new mongoose.Schema(messageFields, {
  timestamps: true,
});
module.exports = mongoose.model("Message", MessageSchema);
