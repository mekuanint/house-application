export default {
  registerOwner(state, payload) {
    state.token = payload;
  },
  setUser(state, payload) {
    state.user = payload;
  },
  login(state, payload) {
    state.token = payload;
  },
};
