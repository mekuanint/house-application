const express = require("express");
const { login, getMe, register, getAll } = require("../controller/auth");
const { protect } = require("../middleware/auth");

const router = express.Router();
router.post("/register", register)
router.post("/login", login);
router.get("/users", getAll);
router.get("/me", protect, getMe);

module.exports = router;
