const ErrorResponse = require("../utils/ErrorResponse");
const asyncHandler = require("../middleware/async");
const Message = require("../models/Message");
const advancedResults = require("../middleware/advancedResults");

// @desc      Add Message
// @route     POST /api/Messages
// @access    Private
exports.addMessage = asyncHandler(async (req, res, next) => {
  req.body.user = req.user.id;

  const message = await Message.create(req.body);
  res.status(200).json({
    success: true,
    data: message,
  });
});

// @desc      Get Messages
// @route     GET /api/Messages
// @access    Public
exports.getMessages = asyncHandler(async (req, res, next) => {
  if (req.params.indId) {
    console.log(indId);
  } else {
    res.status(200).json(res.advancedResults);
  }
});

// @desc      Get single Messages
// @route     GET /api/Messages/:id
// @access    Public
exports.getMessage = asyncHandler(async (req, res, next) => {
  const message = await Message.findById(req.params.id);

  if (!message) {
    return next(
      new ErrorResponse(`No Message with the id of ${req.params.id}`),
      404
    );
  }
  res.status(200).json({
    success: true,
    data: message,
  });
});

// @desc      Update Message
// @route     PUT /api/Messages/:id
// @access    Private
exports.updateMessage = asyncHandler(async (req, res, next) => {
  let message = await Message.findById(req.params.id);

  if (!message) {
    return next(
      new ErrorResponse(`No Message with the id of ${req.params.id}`),
      404
    );
  }
  // Make sure user is shop owner
  if (
    message.user.toString() !== req.user.id &&
    req.user.role !== "admin"
  ) {
    return next(
      new ErrorResponse(
        `User ${req.shop.id} is not authorized to update a Message with id ${req.params.id}`,
        401
      )
    );
  }

  message = await Message.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });

  res.status(200).json({
    success: true,
    data: Message,
  });
});

// @desc      Delete Message
// @route     DELETE /api/Messages/:id
// @access    Private
exports.deleteMessage = asyncHandler(async (req, res, next) => {
  const message = await Message.findById(req.params.id);

  if (!message) {
    return next(
      new ErrorResponse(`No Message with the id of ${req.params.id}`),
      404
    );
  }

  // Make sure user is shop owner
  if (
    message.user.toString() !== req.user.id &&
    req.user.role !== "admin"
  ) {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete Message with id ${req.params.id}`,
        401
      )
    );
  }
  await message.remove();
  res.status(200).json({
    success: true,
    data: {},
  });
});
