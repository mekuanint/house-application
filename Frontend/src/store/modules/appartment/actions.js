import axios from "axios";
const appartment_url = "http://localhost:5000/api/appartments";
const fetch_url = "http://localhost:5000/api/appartments";
const update_url = "http://localhost:5000/api/appartments/:id";
const user_url = "http://localhost:5000/api/auth/users";
export default {
  async loadUsers(context) {
    const response = await axios.get(user_url);
    context.commit("setUsers", response.data.data);
    console.log(response.data.data);
  },
  async addAppartment(context, formData) {
    const realData = {
      name: formData.house,
      description: formData.descr,
      price: formData.price,
      category: formData.category,
      room_no: formData.room_no,
      floor_no: formData.floor_no,
      state: formData.state,
      subcity: formData.subcity,
      wereda: formData.wereda,
    };
    const files = this.selectedFile;

    const token = context.rootGetters.token;
    const response = await axios.post(
      appartment_url,
      { realData, files },
      {
        headers: {
          authorization: "Bearer " + token,
        },
      }
    );
    context.commit("addAppartment", response.data);
  },

  async loadAppartments(context) {
    const response = await axios.get(fetch_url);
    context.commit("setAppartments", response.data.data);
    console.log(response.data);
  },

  async updateAppartments(context, payload) {
    const response = await axios.put(`${update_url}${payload.id}`, payload);
    context.commit("updateAppartment", response.data.data);
  },
};
