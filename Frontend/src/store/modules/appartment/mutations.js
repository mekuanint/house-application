export default {
  addAppartment(state, payload) {
    state.appartments.push(payload);
  },
  setAppartments(state, payload) {
    state.appartments = payload;
  },
  setUsers(state, payload) {
    state.users = payload;
  },
  updateAppartment(state, updatedAppartment) {
    const index = state.appartments.findIndex(
      (appart) => appart.id === updatedAppartment.id
    );
    if (index !== -1) {
      state.appartments.splice(index, 1, updatedAppartment);
    }
  },
};
